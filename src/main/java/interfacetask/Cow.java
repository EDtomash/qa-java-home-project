package interfacetask;

public class Cow implements ShowInfo{

    @Override
    public void showInfo() {
        System.out.println("Cow is fully grown female animal of a domesticated breed of ox, kept to produce milk or beef.\n" +
                "\"a dairy cow\n\"");
    }
}
