package interfacetask;

public class Dog implements ShowInfo{

    @Override
    public void showInfo() {
        System.out.println("Dog is a domesticated carnivorous mammal that typically has a long snout, an acute sense of smell,\n" +
                "nonretractable claws, and a barking, howling, or whining voice.\n");
    }
}
