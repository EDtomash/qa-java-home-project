package interfacetask;

import org.testng.annotations.Test;

public class Cat implements ShowInfo{

    @Override
    public void showInfo() {
        System.out.println("Cat is a small domesticated carnivorous mammal with soft fur, a short snout,\n" +
                "and retractable claws. It is widely kept as a pet or for catching mice, and many breeds have been developed.\n");
    }
}
