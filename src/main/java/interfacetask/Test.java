package interfacetask;

public class Test {
    public static void main(String[] args) {
        Cat cat = new Cat();
        Dog dog = new Dog();
        Cow cow = new Cow();

        cat.showInfo();
        dog.showInfo();
        cow.showInfo();
    }
}
