package javabasictasks;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayListTask {
    @Test
    public static void main(String[] args) {
      //Напишіть програму, яка створює ArrayList і додає в нього 1 000 000 цілих чисел. Потім програма повинна
        // знайти середнє значення чисел у списку і вивести його на консоль.
        //
        //Реалізувати додавання та видалення елементів з середини ArrayList та засікти час роботи
        // (маєте порівняти результати з колегою).
        //
        //Реалізувати додавання та видалення елементів з середини LinkedList та засікти час роботи
        // (маєте порівняти результати з колегою).

        long sumArrayList = 0;
        double avgArrayList = 0;
        long sumLinkedList = 0;
        double avgLinkedList = 0;

        //ArrayList
        ArrayList<Integer> userArrayList = new ArrayList<>();

        for (int i = 0; i <= 1_000_000; i++){
            userArrayList.add(i);
        }

        for (int i = 0; i <= 1_000_000; i++){
            sumArrayList += userArrayList.get(i);
        }

        avgArrayList = (double) sumArrayList / 1_000_000.0;
        System.out.println(avgArrayList);

        long startTimeArray = System.nanoTime();
        userArrayList.add(userArrayList.size() / 2, 50);
        userArrayList.remove(userArrayList.size() / 2);
        long endTimeArray = System.nanoTime();
        long timeAddRemoveArrayList = endTimeArray - startTimeArray;
        System.out.println(timeAddRemoveArrayList);


        //LinkedList
        LinkedList<Integer> userLinkedList = new LinkedList<>();
        for (int p = 0; p <= 1_000_000; p++){
            userLinkedList.add(p);
        }
        for (Integer num : userLinkedList) {
            sumLinkedList += num;
        }

        avgLinkedList = (double) sumLinkedList / 1_000_000.0;
        System.out.println(avgLinkedList);

        long startTimeLinked = System.nanoTime();
        userLinkedList.add(userLinkedList.size() / 2, 50);
        userLinkedList.remove(userLinkedList.size() / 2);
        long endTimeLinked = System.nanoTime();
        long timeAddRemoveLinkedList = endTimeLinked - startTimeLinked;
        System.out.println(timeAddRemoveLinkedList);
    }
}
