package javabasictasks;

public class CarConstructor {
    String model;
    int year;
    double fuel;

    public CarConstructor(String model, int year, double fuel){
        this.model = model;
        this.year = year;
        this.fuel = fuel;
    }
    public void drive(double kilometers) {
        double fuelConsumed = kilometers / 100 * 10;

        if (fuel >= fuelConsumed) {
            System.out.println("Можливо проїхати " + kilometers + " км на автомобілі " + model);
        } else {
            System.out.println("Не достатньо палива для подолання вказаної відстані на автомобілі " + model);
        }
    }

}
