package javabasictasks;

import java.util.Scanner;
public class MultiplicationTable {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Введите число для которого нужна таблица умножения");
        int userNumberInsert = scan.nextInt();

        int i = 0;
        for (i = 0; i < 10; i++) {
            int sum = userNumberInsert * i;
            System.out.println(userNumberInsert + " * " + i + " = " + sum);
        }
    }
}