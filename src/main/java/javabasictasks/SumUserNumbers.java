package javabasictasks;

import org.testng.annotations.Test;

import java.util.Scanner;

public class SumUserNumbers {
    @Test
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int userNumber;
        int sum = 0;

        System.out.print("Введи число. 0 что бы закончить: ");
        userNumber = scan.nextInt();

        while (userNumber != 0){
            sum += userNumber;
            System.out.print("Введи еще раз: ");
            userNumber = scan.nextInt();
            if (userNumber ==0)
                break;
        }
        System.out.println("Сумма всех чисел которые ты ввел = " + sum);
    }
}
