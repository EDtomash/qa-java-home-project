package javabasictasks;

public class Static {
    private static int count = 0;

    public static void incrementCount() {
        count++;
    }

    public static int getStudentsCount () {
        return count;
    }

    public static void main(String[] args) {
        incrementCount();
        incrementCount();
        System.out.println("Total student: " + getStudentsCount());
    }
}
