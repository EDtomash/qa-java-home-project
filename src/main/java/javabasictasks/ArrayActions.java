package javabasictasks;

import org.testng.annotations.Test;

import java.util.Arrays;

public class ArrayActions {
@Test
    public void arrayTask() {
        int[] array69 = {8, 5, 7, 10, 15, 3, 4, 1, 2, 15, 5};
        int sum = 0;
        int min = array69[0];
        int max = 0;
        int spare = 0;

        // Среднее значение
        for (int i = 0; i < array69.length; i++)
            sum += array69[i];
        System.out.println("Your average is: " + (double) sum / array69.length);

        //Максимальный элемент
        for (int j = 0; j < array69.length; j++)
            if (max < array69[j])
                max = array69[j];
        System.out.println("Your max is: " + max);

        //Минимальный элемент
        for (int k = 0; k < array69.length; k++)
            if (min > array69[k])
                min = array69[k];
        System.out.println("Your min is: " + min);

        //Дупликат
        for (int o = 0; o < array69.length - 1; o++) {
            for (int t = o + 1; t < array69.length; t++) {
                if (array69[o] == array69[t])
                    System.out.println("Your array contains spare: " + array69[o] + " and " + array69[t]);
            }
        }
        // Виведення відсортованого масиву
        for (int i = 0; i < array69.length - 1; i++) {
            for (int j = 0; j < array69.length - i - 1; j++) {
                if (array69[j] < array69[j + 1]) {
                    int temp = array69[j];
                    array69[j] = array69[j + 1];
                    array69[j + 1] = temp;
                }
            }
        }
        System.out.println("Sorted array in descending order: " + Arrays.toString(array69));
    }
}
