package javabasictasks;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class IconFinderPage {

        private final SelenideElement ICONS_MENU_BTN = $(By.xpath("//li[@class='nav-item']//a[contains(text(),'Icons')]"));
        private final SelenideElement CATEGORY_BTN = $(By.xpath("//img[@alt='Feather - 286 icons']"));
        private final SelenideElement PRICE_FREE_RADIO_BTN = $(By.xpath("//label[@for='price-free']"));
        private final SelenideElement SELECT_ICON = $(By.xpath("//img[@alt='phone ']"));
        private final SelenideElement SELECT_PX = $(By.xpath("//a[contains(text(),'128 px')]"));
        private final SelenideElement DOWNLOAD_BTN = $(By.xpath("//a[@class='btn btn-block btn-primary py-3 download-link-1-8666632-128-False']"));

    public IconFinderPage clickIconMenu() {
        ICONS_MENU_BTN.shouldBe(visible).click();
        return this;
    }

    public IconFinderPage clickCategoryBtn() {
        CATEGORY_BTN.shouldBe(visible).click();
        return this;
    }

    public IconFinderPage clickPriceFreeRadioBtn() {
        PRICE_FREE_RADIO_BTN.shouldBe(visible).click();
        return this;
    }

    public IconFinderPage clickIconBtn() {
        SELECT_ICON.shouldBe(visible).click();
        return this;
    }

    public IconFinderPage clickPXBtn() {
        SELECT_PX.shouldBe(visible).click();
        return this;
    }

    public File clickDownloadBtn() {
        DOWNLOAD_BTN.shouldBe(visible);
        return DOWNLOAD_BTN.download();
    }

}
