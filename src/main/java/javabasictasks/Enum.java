package javabasictasks;

import java.time.DayOfWeek;

public class Enum {
    enum DaysOfWeek {
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY,
        SUNDAY
    }
    public static void main(String[] args) {
        DayOfWeek today = DayOfWeek.SUNDAY;
        checkDay(today);
    }
    public static void checkDay(DayOfWeek today){
        switch (today) {
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                System.out.println(today + " is working day");
                break;
            case SATURDAY:
            case SUNDAY:
                System.out.println(today + " is weekend my dude");
                break;
            default:
                System.out.println("Invalid day.");
                break;
        }
    }
}
