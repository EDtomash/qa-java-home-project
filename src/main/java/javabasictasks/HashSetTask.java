package javabasictasks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

public class HashSetTask {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введіть рядок тексту: ");
        String text = scanner.nextLine();

        Map<String, Integer> wordCountMap = new HashMap<>();
        HashSet<String> uniqueWords = new HashSet<>();

        for (String word : text.split("\\s+")) {
            wordCountMap.put(word, wordCountMap.getOrDefault(word, 0) + 1);
            uniqueWords.add(word);
        }

        System.out.println("Кількість унікальних слів: " + uniqueWords.size());

        System.out.println("\nКількість кожного слова:");
        for (Map.Entry<String, Integer> entry : wordCountMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        scanner.close();
    }
}
