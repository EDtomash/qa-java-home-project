package javabasictasks;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        double result = 0;

        System.out.println("Введи первое число");
        double userNumber1 = scan.nextDouble();

        System.out.println("Выбери операцию: +, -, /, *");
        String userOperation = scan.next();

        System.out.println("Выбери второе число");
        double userNumber2 = scan.nextDouble();

        switch (userOperation) {
            case "+":
                result = userNumber1 + userNumber2;
                break;
            case "-":
                result = userNumber1 - userNumber2;
                break;
            case "*":
                result = userNumber1 * userNumber2;
                break;
            case "/":
                if (userNumber2 == 0) {
                    System.out.println("Не дели сука на ноль");
                } else {
                    result = userNumber1 / userNumber2;
                }
                break;
            default:
                System.out.println("Неверная операция");
                return;
        }
        System.out.println("Результат " + result);
    }
}
