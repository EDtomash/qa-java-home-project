package javabasictasks;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashMapTask1 {
    public static void main(String[] args) {
        Map<String, Integer> inventory = new HashMap<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\nОберіть дію:");
            System.out.println("1. Додати об'єкт");
            System.out.println("2. Видалити об'єкт");
            System.out.println("3. Показати інвентар");
            System.out.println("4. Вихід");

            int choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.print("Введіть назву об'єкту: ");
                    String itemName = scanner.nextLine();
                    System.out.print("Введіть кількість: ");
                    int quantity = scanner.nextInt();
                    inventory.put(itemName, inventory.getOrDefault(itemName, 0) + quantity);
                    break;
                case 2:
                    System.out.print("Введіть назву об'єкту для видалення: ");
                    itemName = scanner.nextLine();
                    if (inventory.containsKey(itemName)) {
                        inventory.remove(itemName);
                        System.out.println("Об'єкт видалено.");
                    } else {
                        System.out.println("Такого об'єкту немає в інвентарі.");
                    }
                    break;
                case 3:
                    System.out.println("\nІнвентар:");
                    for (Map.Entry<String, Integer> entry : inventory.entrySet()) {
                        System.out.println(entry.getKey() + ": " + entry.getValue());
                    }
                    break;
                case 4:
                    System.out.println("Вихід з програми.");
                    scanner.close();
                    return;
                default:
                    System.out.println("Невірний вибір.");
            }
        }
    }
}
