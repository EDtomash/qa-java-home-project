package javabasictasks;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HashMapTask2 {

    enum Subject {
        MATH, SCIENCE, HISTORY, ENGLISH
    }

    public static void main(String[] args) {
        Map<Subject, Integer> grades = new HashMap<>();
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("\nОберіть предмет:");
            for (Subject subject : Subject.values()) {
                System.out.println(subject.ordinal() + 1 + ". " + subject);
            }
            System.out.println(Subject.values().length + 1 + ". Вихід");

            int choice = scanner.nextInt();
            if (choice == Subject.values().length + 1) {
                break;
            }

            Subject selectedSubject = Subject.values()[choice - 1];
            System.out.print("Введіть оцінку для " + selectedSubject + ": ");
            int grade = scanner.nextInt();
            grades.put(selectedSubject, grade);
        }

        System.out.println("\nОцінки:");
        for (Map.Entry<Subject, Integer> entry : grades.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

        scanner.close();
    }
}
