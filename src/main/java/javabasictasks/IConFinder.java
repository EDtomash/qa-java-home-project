package javabasictasks;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;

import static com.codeborne.selenide.Selenide.*;

public class IConFinder extends IconFinderPage {

        @BeforeMethod
        public void setup() {
            Configuration.browserSize = "1920x1080";
            open("https://www.iconfinder.com/");
        }

        @Test
        public void iconFinderTest() {
            File downloadIcon = new IconFinderPage()
                    .clickIconMenu()
                    .clickCategoryBtn()
                    .clickPriceFreeRadioBtn()
                    .clickIconBtn()
                    .clickPXBtn()
                    .clickDownloadBtn();

            if(downloadIcon.delete()) {
                System.out.println("File not found");
            } else {
                System.out.println("Failed to delete file");
            }
        }



}
