package pages.demowebshop;

public class HomePage extends AbstractBasePage<HomePage> {

    public void subscribeToNewsletter(String email) {
        getNewsLetterSubscribeBlock()
                .enterEmail(email)
                .clickSubscribeBtn();
    }
}
