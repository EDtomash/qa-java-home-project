package pages.demowebshop;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.interactable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.*;

public class NewsLetterSubscribeBlock extends AbstractBasePage<NewsLetterSubscribeBlock>{

    private final By SUBSCRIBE_EMAIL_FIELD = id("newsletter-email");
    private final By NEWSLETTER_SUBSCRIBE_BTN = id("newsletter-subscribe-button");
    private final By NEWSLETTER_RESULT_MSG = id("newsletter-result-block");

    public NewsLetterSubscribeBlock enterEmail(String email) {
        $(SUBSCRIBE_EMAIL_FIELD)
                .shouldBe(visible, interactable)
                .sendKeys(email);
        return this;
    }

    public NewsLetterSubscribeBlock clickSubscribeBtn() {
        $(NEWSLETTER_SUBSCRIBE_BTN)
                .shouldBe(visible, interactable)
                .click();
        return this;
    }

    public String getNewsLetterResultMsg() {
       return $(NEWSLETTER_RESULT_MSG).getText();
    }
}
