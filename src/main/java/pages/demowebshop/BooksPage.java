package pages.demowebshop;


import static com.codeborne.selenide.Selenide.$x;

public class BooksPage extends AbstractBasePage<BooksPage> {

    private final String BOOK_ADD_TO_CART_BTN = "//div[@class='product-grid']//a[contains(text(), '%s')]/../..//input";

    public BooksPage addBookToCart(String bookTitle) {
        $x(String.format(BOOK_ADD_TO_CART_BTN, bookTitle)).click();
        return this;
    }
}

