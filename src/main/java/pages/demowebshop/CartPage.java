package pages.demowebshop;

import static com.codeborne.selenide.Selenide.$x;
import static java.lang.String.format;

public class CartPage extends AbstractBasePage<CartPage> {

    private final String ITEM_CART_LIST = "//tr//a[text()='%s']";

    public boolean isItemPresentInCart(String itemName) {
        return $x(format(ITEM_CART_LIST, itemName)).isDisplayed();
    }
}
