package pages.demowebshop;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.interactable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static org.openqa.selenium.By.*;

public class LoginPage  extends AbstractBasePage<LoginPage>{

    private final By EMAIL_FIELD = id("Email");
    private final By PASSWORD_FIELD = id("Password");
    private final By REMEMBER_ME_CHECKBOX = id("RememberMe");
    private final By LOGIN_BTN = xpath("//input[@class='button-1 login-button']");

    public LoginPage setEmail(String email) {
        $(EMAIL_FIELD)
                .shouldBe(visible, interactable)
                .sendKeys(email);
        return new LoginPage();
    }

    public LoginPage setPassword(String password) {
        $(PASSWORD_FIELD)
                .shouldBe(visible, interactable)
                .sendKeys(password);
        return new LoginPage();
    }

    public LoginPage selectRememberMeCheckBox() {
        $(REMEMBER_ME_CHECKBOX)
                .shouldBe(visible, interactable)
                .click();
        return new LoginPage();
    }

    public HomePage submitLoginBtn() {
        $(LOGIN_BTN)
                .shouldBe(visible, interactable)
                .click();
        return new HomePage();
    }
}
