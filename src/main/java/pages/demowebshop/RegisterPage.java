package pages.demowebshop;


import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class RegisterPage extends AbstractBasePage<RegisterPage>{

    private final By FIRST_NAME_FIELD = By.id("FirstName");
    private final By LAST_NAME_FIELD = By.id("LastName");
    private final By EMAIL_FIELD = By.id("Email");
    private final By PASSWORD_FIELD = By.id("Password");
    private final By CONFIRM_PASSWORD_FIELD = By.name("ConfirmPassword");
    private final By REGISTER_BTN = By.id("register-button");
    private final String GENDER = "//label[@for='gender-%s']";

    public RegisterPage fillFirstName(String firstName) {
        $(FIRST_NAME_FIELD).sendKeys(firstName);
        return this;
    }

    public RegisterPage fillLastName(String lastName) {
        $(LAST_NAME_FIELD).sendKeys(lastName);
        return this;
    }

    public RegisterPage fillEmail(String email) {
        $(EMAIL_FIELD).sendKeys(email);
        return this;
    }

    public RegisterPage fillPassword(String password) {
        $(PASSWORD_FIELD).sendKeys(password);
        return this;
    }

    public RegisterPage confirmPasswordField(String password) {
        $(CONFIRM_PASSWORD_FIELD).sendKeys(password);
        return this;
    }

    public RegisterResultPage submitForm() {
        $(REGISTER_BTN).click();
        return new RegisterResultPage();
    }

    public RegisterPage chooseGender(String gender) {
        $x(String.format(GENDER, gender)).click();
        return this;
    }
}
