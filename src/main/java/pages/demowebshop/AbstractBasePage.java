package pages.demowebshop;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.interactable;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static java.time.Duration.ofSeconds;
import static org.openqa.selenium.By.*;

public abstract class AbstractBasePage<T extends AbstractBasePage> {

    private final By FACEBOOK_NAV_LINK = xpath("//li[@class='facebook']/a");
    private final By REGISTER_PAGE_LINK = xpath("//div[@class='header-links']//a[text()='Register']");
    private final By LOGIN_PAGE_NAV_LINK = xpath("//div[@class='header-links']//a[contains(text(), 'Log in')]");
    private final By BOOKS_PAGE_NAV_LINK = xpath("//div[@class='listbox']//a[contains(text(), 'Books')]");
    private final By SHOPPING_CART_NAV_LINK = id("topcartlink");
    private final By CUSTOMER_EMAIL_TO_CABINET_LINK = xpath("//div[@class='header-links']//a[@class='account']");

    public FacebookPage openFacebook() {
        $(FACEBOOK_NAV_LINK)
                .shouldBe(visible, interactable)
                .click();
        return new FacebookPage();
    }

    public RegisterPage openRegisterPage() {
        $(REGISTER_PAGE_LINK)
                .shouldBe(visible, interactable)
                .click();
        return new RegisterPage();
    }

    public LoginPage openLoginPage() {
        $(LOGIN_PAGE_NAV_LINK)
                .shouldBe(visible, interactable)
                .click();
        return new LoginPage();
    }

    public String getEmailTextFromHeader() {
        return $(CUSTOMER_EMAIL_TO_CABINET_LINK).getText();
    }

    public BooksPage openBooksPage() {
        $(BOOKS_PAGE_NAV_LINK)
                .shouldBe(visible, interactable)
                .click();
        return new BooksPage();
    }

    public CartPage openCartPage() {
        $(SHOPPING_CART_NAV_LINK)
                .shouldBe(visible, interactable)
                .click();
        return new CartPage();
    }

    public NewsLetterSubscribeBlock getNewsLetterSubscribeBlock() {
        return new NewsLetterSubscribeBlock();
    }

    public T waitTillLoadingBlockDisappear() {
        $x("//div[@class='ajax-loading-block-window' and not contains(@style,'block')]")
                .shouldNot(visible, ofSeconds(5));

        return (T) this;
    }

    public T waitTillLoadingBlockAppear() {
        $x("//div[@class='ajax-loading-block-window' and contains(@style,'none')]")
                .shouldBe(visible, ofSeconds(5));
        return (T) this;
    }

    public T waitTillLoadingBlock() {
        waitTillLoadingBlockAppear();
        waitTillLoadingBlockDisappear();
        return (T) this;
    }
}
