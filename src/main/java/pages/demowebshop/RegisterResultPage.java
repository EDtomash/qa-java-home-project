package pages.demowebshop;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class RegisterResultPage extends AbstractBasePage<RegisterResultPage>{

    private static final By RESULT = By.className("result");

    public static String getNotificationMessage() {
        return $(RESULT).getText();
    }
}
