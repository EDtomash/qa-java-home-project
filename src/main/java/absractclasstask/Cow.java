package absractclasstask;

public class Cow extends AbstractAnimal {

    @Override
    public void makeSound() {
        System.out.println("Mooo");
    }
}
