package demoqa;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class TextBox {
    WebDriver driver;

@Test
    public void credentials() throws InterruptedException {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    driver.get("https://demoqa.com/text-box");

    JavascriptExecutor jse = (JavascriptExecutor)driver;

    String userName = "Stepan Giga";
    String userEmail = "stepan@giga.com";
    String userAddress = "1707 Teakwood Dr, TX, 75098";
    String userPermanentAddress = "1707 Teakwood Dr, TX, 75098";

    Thread.sleep(2000);

    WebElement fullName = driver.findElement(By.xpath("//form[@id='userForm']//input[@id='userName']"));
    WebElement email = driver.findElement(By.xpath("//form[@id='userForm']//input[@id='userEmail']"));
    WebElement currentAddress = driver.findElement(By.xpath("//form[@id='userForm']//textarea[@id='currentAddress']"));
    WebElement permanentAddress = driver.findElement(By.xpath("//form[@id='userForm']//textarea[@id='permanentAddress']"));
    WebElement submitButton = driver.findElement(By.xpath("//form[@id='userForm']//button[@id='submit']"));

    fullName.sendKeys(userName);
    email.sendKeys(userEmail);
    currentAddress.sendKeys(userAddress);
    permanentAddress.sendKeys(userPermanentAddress);

    jse.executeScript("arguments[0].scrollIntoView();", submitButton);
    submitButton.click();

    List<WebElement> outputCredentials = driver.findElements(By.xpath("//div[@id='output']//p"));

    Assert.assertEquals("Name:" + userName, outputCredentials.get(0).getText());
    Assert.assertEquals("Email:" + userEmail, outputCredentials.get(1).getText());
    Assert.assertEquals("Current Address :" + userAddress, outputCredentials.get(2).getText());
    Assert.assertEquals("Permananet Address :" + userPermanentAddress, outputCredentials.get(3).getText());

    driver.quit();
}
}
