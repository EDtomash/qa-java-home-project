package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RadioButton {
    WebDriver driver;

    @Test
    public void radioBtn() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/radio-button");

        WebElement yesBtn = driver.findElement(By.xpath("//div/label[@for='yesRadio']"));
        WebElement impressiveBtn = driver.findElement(By.xpath("//input[@id='impressiveRadio']"));

        yesBtn.click();

        WebElement outputInfo = driver.findElement(By.xpath("//p[@class='mt-3']/span"));

        Assert.assertEquals(yesBtn.getText(), outputInfo.getText());

        driver.quit();
    }
}
