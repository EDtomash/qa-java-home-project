package demoqa;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Alerts {
    @Test
    public void alertsActions() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/alerts");
        String name = "Stepan Giga";

        WebElement alertButton = driver.findElement(By.xpath("//button[@id='alertButton']"));
        WebElement timerAlertButton = driver.findElement(By.xpath("//button[@id='timerAlertButton']"));
        WebElement confirmButton = driver.findElement(By.xpath("//button[@id='confirmButton']"));
        WebElement promptButton = driver.findElement(By.xpath("//button[@id='promtButton']"));

        alertButton.click();
        driver.switchTo().alert().accept();

        timerAlertButton.click();
        Thread.sleep(5000);
        driver.switchTo().alert().accept();

        confirmButton.click();
        driver.switchTo().alert().accept();
        WebElement resultAfterAlert = driver.findElement(By.xpath("//span[@id='confirmResult']"));
        Assert.assertTrue(resultAfterAlert.getText().contains("Ok"));

        promptButton.click();
        Alert promptAlert = driver.switchTo().alert();
        promptAlert.sendKeys(name);
        promptAlert.accept();

        WebElement enteredData = driver.findElement(By.xpath("//span[@id='promptResult']"));
        Assert.assertTrue(enteredData.getText().contains(name));

        driver.quit();
    }
}
