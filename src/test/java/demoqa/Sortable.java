package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class Sortable {
    @Test
    public void reverseSort() {
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.get("https://demoqa.com/sortable");


        List<WebElement> oneToSix = driver.findElements(By.xpath("//div[@class='vertical-list-container mt-4']/div"));

        Actions action = new Actions(driver);

        for (int i = 0; i < oneToSix.size() - 1; i++) {
            action.dragAndDrop(oneToSix.get(5), oneToSix.get(i)).build().perform();
        }

        Assert.assertEquals(oneToSix.get(0).getText(), "Six");
        Assert.assertEquals(oneToSix.get(1).getText(), "Five");
        Assert.assertEquals(oneToSix.get(2).getText(), "Four");
        Assert.assertEquals(oneToSix.get(3).getText(), "Three");
        Assert.assertEquals(oneToSix.get(4).getText(), "Two");
        Assert.assertEquals(oneToSix.get(5).getText(), "One");

        driver.quit();

    }
}

