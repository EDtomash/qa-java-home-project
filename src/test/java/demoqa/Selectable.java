package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class Selectable {
    WebDriver driver;
    @Test
    public void select() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.get("https://demoqa.com/selectable");

        List<WebElement> selectableRow = driver.findElements(By.xpath("//div[@id='listContainer']//ul/li"));

        WebElement selectedElement = selectableRow.get(3);
        selectedElement.click();

        String classAttributeValue = selectedElement.getAttribute("class");

        Assert.assertTrue(classAttributeValue.contains("active"), "Element is not selected");

        driver.quit();
    }

}
