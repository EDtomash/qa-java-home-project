package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ModalDialogs {

    @Test
        public void modal() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/modal-dialogs");

        WebElement smallModalButton = driver.findElement(By.xpath("//button[@id='showSmallModal']"));
        WebElement largeModalButton = driver.findElement(By.xpath("//button[@id='showLargeModal']"));

        smallModalButton.click();
        WebElement smallWindow = driver.findElement(By.xpath("//div[@id='example-modal-sizes-title-sm']"));
        Assert.assertEquals(smallWindow.getText(), "Small Modal");

        WebElement closeButton = driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        closeButton.click();

        largeModalButton.click();
        WebElement largeWindow = driver.findElement(By.xpath("//div[@id='example-modal-sizes-title-lg']"));
        Assert.assertEquals(largeWindow.getText(), "Large Modal");

        WebElement closeBtn = driver.findElement(By.xpath("//button[@id='closeLargeModal']"));
        closeBtn.click();

        driver.quit();




        
    }
}
