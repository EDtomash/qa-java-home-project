package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.file.WatchEvent;

public class WebTables {

    @Test
    public void tableActions() throws InterruptedException {
        String firstName = "Stepan";
        String lastName = "Giga";
        String email = "stepangiga@gmail.com";
        String age = "16";
        String salary = "5000";
        String department = "Labor Crew";

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/webtables");

        WebElement addButton = driver.findElement(By.xpath("//button[@id='addNewRecordButton']"));
        WebElement searchField = driver.findElement(By.xpath("//input[@id='searchBox']"));
        WebElement searchButton = driver.findElement(By.xpath("//span[@id='basic-addon2']"));

        addButton.click();


        WebElement firstNameField = driver.findElement(By.xpath("//input[@id='firstName']"));
        WebElement lastNameField = driver.findElement(By.xpath("//input[@id='lastName']"));
        WebElement emailField = driver.findElement(By.xpath("//input[@id='userEmail']"));
        WebElement ageField = driver.findElement(By.xpath("//input[@id='age']"));
        WebElement salaryField = driver.findElement(By.xpath("//input[@id='salary']"));
        WebElement departmentField = driver.findElement(By.xpath("//input[@id='department']"));
        WebElement submitButton = driver.findElement(By.xpath("//button[@id='submit']"));

        firstNameField.sendKeys(firstName);
        lastNameField.sendKeys(lastName);
        emailField.sendKeys(email);
        ageField.sendKeys(age);
        salaryField.sendKeys(salary);
        departmentField.sendKeys(department);
        submitButton.submit();

        Thread.sleep(1000);

        searchField.sendKeys(firstName);
        searchButton.click();
        WebElement searchResult = driver.findElement(By.xpath("//div[@class='rt-tr -odd']/div[1]"));

        Assert.assertEquals(searchResult.getText(), firstName);

        driver.quit();
    }
}
