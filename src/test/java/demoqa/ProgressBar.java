package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProgressBar {
    @Test
    public void progressBar() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/progress-bar");

        WebElement startButton = driver.findElement(By.xpath("//button[@id='startStopButton']"));
        startButton.click();

        Thread.sleep(10000);

        WebElement resetButton = driver.findElement(By.xpath("//button[@id='resetButton']"));

        Assert.assertTrue(resetButton.isDisplayed());

        driver.quit();

    }
}
