package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class UploadAndDownload {

@Test
    public void uploadDownload() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/upload-download");

        WebElement downloadButton = driver.findElement(By.xpath("//a[@id='downloadButton']"));
        WebElement chooseFileButton = driver.findElement(By.xpath("//input[@type='file']"));

        downloadButton.click();
        Thread.sleep(3000);
        chooseFileButton.sendKeys("C:\\Users\\EDtomash\\Downloads\\sampleFile.jpeg");

        WebElement filePath = driver.findElement(By.xpath("//p[@id='uploadedFilePath']"));

        Assert.assertTrue(filePath.getText().contains("sampleFile.jpeg"));

        driver.quit();
    }
}
