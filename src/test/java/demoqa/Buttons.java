package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
 public class Buttons {
    @Test
    public void clicks() throws InterruptedException {
      WebDriver driver = new ChromeDriver();
      driver.manage().window().maximize();
      driver.get("https://demoqa.com/buttons");
      Thread.sleep(2000);


      WebElement doubleClickBtn = driver.findElement(By.xpath("//button[@id='doubleClickBtn']"));
      WebElement rightClickBtn = driver.findElement(By.xpath("//button[@id='rightClickBtn']"));
      WebElement clickMeBtn = driver.findElement(By.xpath("//button[text()='Click Me']"));

      Actions actions = new Actions(driver);
      actions.doubleClick(doubleClickBtn).build().perform();
      actions.contextClick(rightClickBtn).build().perform();
      clickMeBtn.click();

      WebElement doubleClickMessage = driver.findElement(By.xpath("//p[@id='doubleClickMessage']"));
      Assert.assertEquals(doubleClickMessage.getText(), "You have done a double click");

      WebElement rightClickMessage = driver.findElement(By.xpath("//p[@id='rightClickMessage']"));
      Assert.assertEquals(rightClickMessage.getText(), "You have done a right click");

      WebElement clickMessage = driver.findElement(By.xpath("//p[@id='dynamicClickMessage']"));
      Assert.assertEquals(clickMessage.getText(), "You have done a dynamic click");

      driver.quit();

    }
}
