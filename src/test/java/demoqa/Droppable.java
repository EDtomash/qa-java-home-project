package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Droppable {
    WebDriver driver;
    @Test
    public void simpleTab() throws InterruptedException {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/droppable");

        WebElement dragMe = driver.findElement(By.xpath("//div[@id='draggable']"));
        WebElement dropHere = driver.findElement(By.xpath("//div[@class='simple-drop-container']//div[@id='droppable']"));

        Thread.sleep(1000);

        Actions action = new Actions(driver);
        action.dragAndDrop(dragMe, dropHere).build().perform();

        String expectedResult = "Dropped!";
        String actualResult = dropHere.getText();

        Assert.assertEquals(actualResult, expectedResult, "The element was not successfully dragged");

        driver.quit();
    }
}
