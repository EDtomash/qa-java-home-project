package demoqa;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Set;

public class BrowserWindows {

    @Test
    public void newTabOpener() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/browser-windows");

        String currentHandle = driver.getWindowHandle();

       WebElement newTab = driver.findElement(By.xpath("//button[@id='tabButton']"));
       newTab.click();

        Set<String> handles = driver.getWindowHandles();
        for (String handle : handles) {
            if (!handle.equals(currentHandle)) {
                driver.switchTo().window(handle);
                break;
            }
        }

        WebElement newTabH1 = driver.findElement(By.xpath("//h1[@id='sampleHeading']"));
        Assert.assertEquals(newTabH1.getText(), "This is a sample page");

        driver.switchTo().window(currentHandle);
        driver.quit();
    }

@Test
    public void newWindowOpener() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/browser-windows");

        String currentHandle = driver.getWindowHandle();

        WebElement newWindowButton = driver.findElement(By.xpath("//button[@id='windowButton']"));
        newWindowButton.click();

         Set<String> handles = driver.getWindowHandles();
         for (String handle : handles) {
             if (!handle.equals(currentHandle)) {
            driver.switchTo().window(handle);
            break;
           }
         }

         WebElement newWindow = driver.findElement(By.xpath("//h1[@id='sampleHeading']"));
         Assert.assertEquals(newWindow.getText(), "This is a sample page");
         driver.close();
          driver.quit();
    }
    @Test
    public void newWindowMessage() {
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://demoqa.com/browser-windows");

        String currentHandle = driver.getWindowHandle();
        WebElement newWindowMessage = driver.findElement(By.xpath("//button[@id='messageWindowButton']"));
        newWindowMessage.click();

        Set<String> handles = driver.getWindowHandles();
        for (String handle : handles) {
            if (!handle.equals(currentHandle)) {
                driver.switchTo().window(handle);
               break;
            }
        }


        WebElement newWindowMessageNext = driver.findElement(By.xpath("/html/body"));
        Assert.assertTrue(newWindowMessageNext.getText().contains("Knowledge increases"), "Error");
        driver.close();
        driver.quit();
    }
}
