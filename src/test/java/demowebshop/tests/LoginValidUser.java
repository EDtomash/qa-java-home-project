package demowebshop.tests;

import org.testng.annotations.Test;
import pages.demowebshop.HomePage;

import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.*;

public class LoginValidUser extends InitialTest {

    @Test
    public void loginValidUser() {
        open("https://demowebshop.tricentis.com/");

        String email = "zalupa1408@gmail.com";
        String password = "12301230";

        String actualEmail = new HomePage()
                .openLoginPage()
                .setEmail(email)
                .setPassword(password)
                .selectRememberMeCheckBox()
                .submitLoginBtn()
                .getEmailTextFromHeader();

        assertEquals(email, actualEmail);
    }
}
