package demowebshop.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.demowebshop.FacebookPage;
import pages.demowebshop.HomePage;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchTo;
import static org.testng.Assert.*;

public class FacebookRedirecting extends InitialTest {

    @Test
    public void openFacebookPage() {
        open("https://demowebshop.tricentis.com/");

        new HomePage().openFacebook();

        String facebookPageTitle = switchTo().window(1).getTitle();

        assertEquals(facebookPageTitle, "NopCommerce | Facebook");
    }
}
