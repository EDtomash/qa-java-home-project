package demowebshop.tests;

import com.codeborne.selenide.Configuration;
import net.datafaker.Faker;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static com.codeborne.selenide.Selenide.closeWebDriver;


public class InitialTest {
    protected final Faker faker = new Faker();

    @BeforeMethod
    public void setup() {
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.headless = false;
    }

    @AfterMethod
    public void tearDown() {
        closeWebDriver();
    }

}
