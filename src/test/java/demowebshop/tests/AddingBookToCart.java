package demowebshop.tests;

import org.testng.annotations.Test;
import pages.demowebshop.HomePage;

import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.assertTrue;

public class AddingBookToCart extends InitialTest {

    @Test
    public void addingBookToCart() {
        open("ope");
        String bookTitle = "Health Book";

        var isBookAdded = new HomePage()
                .openBooksPage()
                .addBookToCart(bookTitle)
                .waitTillLoadingBlock()
                .openCartPage()
                .isItemPresentInCart(bookTitle);

        assertTrue(isBookAdded, "The book has not been added to the cart");
    }
}


