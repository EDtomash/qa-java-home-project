package demowebshop.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.demowebshop.HomePage;

import static com.codeborne.selenide.Selenide.open;
import static pages.demowebshop.RegisterResultPage.getNotificationMessage;

public class NewUserRegistration extends InitialTest {

    @Test
    public void registrationTest() {
        open("https://demowebshop.tricentis.com/");
        String password = faker.internet().password();

        new HomePage()
                .openRegisterPage()
                .fillFirstName(faker.name().firstName())
                .fillLastName(faker.name().lastName())
                .fillEmail(faker.internet().emailAddress())
                .fillPassword(password)
                .confirmPasswordField(password)
                .chooseGender("male")
                .submitForm();

        String expectedResultMsg = "Your registration completed";
        Assert.assertEquals(expectedResultMsg, getNotificationMessage());
    }
}
