package demowebshop.tests;

import org.testng.annotations.Test;
import pages.demowebshop.HomePage;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.switchTo;
import static org.testng.Assert.*;

public class SubscribeForNewsletter extends InitialTest{

    @Test
    public void subscribeForNewsLetter() {
        open("https://demowebshop.tricentis.com/");

        String email = "zalupa1408@gmail.com";

       String subscribeResult = new HomePage()
               .getNewsLetterSubscribeBlock()
               .enterEmail(email)
               .clickSubscribeBtn()
               .getNewsLetterResultMsg();

        String expectedResult = "Thank you for signing up! A verification email has been sent. We appreciate your interest.";
        assertEquals(subscribeResult, expectedResult);
    }
}
