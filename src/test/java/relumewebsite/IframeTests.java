package relumewebsite;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;

public class IframeTests {

    SelenideElement getStartedBtn = $x("//a[@class='fixed-cta migration w-inline-block']");
    SelenideElement monoboardLink = $x("//a[contains(text(), 'Relume Moodboard Tool')]");
    SelenideElement giveItATryBtn = $x("//div[@class='horizontal-flex-div centre-div']/a[contains(text(), 'give it a try')]");
    SelenideElement monoboardH1 = $x("//h1[@class='header-title margin-0']");

    @BeforeMethod
    public void setup(){
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.headless = false;
        open("https://webflow.com/made-in-webflow/website/relume-cloneable");
    }
    @AfterMethod
    public void tearDown() {
        closeWebDriver();
    }

    @Test
    public void visibilityOfGettingStartedBtn() {
        switchTo().frame(0);
        Assert.assertTrue(getStartedBtn.isDisplayed());
    }

    @Test
    public void findMonoboardExplorationTitle() {
        monoboardLink.click();
        switchTo().window(1);
        switchTo().frame(0);
        giveItATryBtn.click();

        Assert.assertEquals(monoboardH1.getText(), "Moodboard Exploration");
    }
}
